# steps for setup

1. Install Laravel Project
2. Create Database in mySql
3. Update details on .env file
    # CREATE USER 'onlineLeaveSystem'@'localhost' IDENTIFIED BY 'onlineLeaveSystem';
    # GRANT ALL PRIVILEGES ON *.* TO 'onlineLeaveSystem'@'localhost' WITH GRANT OPTION;
4. Install the spatie for role and permission
    # composer require spatie/laravel-permission
5. Add spatie service provider to config/app.php
6. Run below command to add config/permission.php and migration for spatie
    # php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"
7. Clear cache using below commands
    # php artisan optimize:clear
    # php artisan config:clear
8. Run  php artisan migrate

# Application level setup

1. We have to use seeder to add default system permissions. You can do it by first creating seeder file.
    # php artisan make:seeder SystemDefaultSeeder
2. Add the Default roles, Permissions and then run seeder with below command
    # php artisan db:seed --class=SystemDefaultSeeder