<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SystemDefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Default Permissions
        //User Permissions
        Permission::create(['name' => 'create user']);
        Permission::create(['name' => 'delete user']);
        Permission::create(['name' => 'update user']);

        //Leave Type Permission
        Permission::create(['name' => 'create leave type']);
        Permission::create(['name' => 'delete leave type']);
        Permission::create(['name' => 'update leave type']);

        //Leave Permission
        Permission::create(['name' => 'create leave']);
        Permission::create(['name' => 'cancel leave']);
        Permission::create(['name' => 'update leave']);

        //Create Supervisor Role
        $supervisor_Role = Role::create(['name' => 'supervisor']);
        //Give User Permission
        $supervisor_Role->givePermissionTo('create user');
        $supervisor_Role->givePermissionTo('delete user');
        $supervisor_Role->givePermissionTo('update user');
        //Give Leave Type Permission
        $supervisor_Role->givePermissionTo('create leave type');
        $supervisor_Role->givePermissionTo('delete leave type');
        $supervisor_Role->givePermissionTo('update leave type');
        //Give Leave Permission
        $supervisor_Role->givePermissionTo('update leave');

        //Create Staff Role
        $staff_Role = Role::create(['name' => 'staff']);
        $supervisor_Role->givePermissionTo('create leave');
        $supervisor_Role->givePermissionTo('cancel leave');

        //create Main supervisor
        $user = \App\Models\User::factory()->create([
            'name' => 'Supervisor 1',
            'email' => 'supervisor@swipewire.com',
            'password' => bcrypt('password'),
        ]);
    }
}
